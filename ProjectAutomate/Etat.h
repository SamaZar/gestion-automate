#pragma once
#include"Automate.h"
#include"Transition.h"

class Etat{

protected:
	int m_indexEtat;
	bool m_initial, m_acceptant;
	std::vector<Transition*> m_vers_Transition;
public:
	//Constructeurs
	Etat();
	Etat(int indexEtat, bool m_initial, bool acceptant);
	~Etat();

	//les getters
	int getMindexEtat();
	bool getMinitial();
	bool getMacceptant();
	std::vector<Transition*>& get_vers_Transition();

	//les setters
	void setMindexEtat(int MindexEtat);
	void setMinitial(bool Minitant);
	void setMacceptant(bool Macceptant);
	void set_vers_transition(std::vector<Transition*> &Mvers_Transition);

};
