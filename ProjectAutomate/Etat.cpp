#include"Etat.h"

Etat::Etat(){
}

Etat::Etat(int indexEtat, bool initial, bool acceptant) {
	m_indexEtat = indexEtat;
	m_initial = initial;
	m_acceptant = acceptant;
}

Etat::~Etat() {

}

//les getters
int Etat::getMindexEtat() {
	return m_indexEtat;
}

bool Etat::getMinitial() {
	return m_initial;
}

bool Etat::getMacceptant() {
	return m_acceptant;
}

std::vector<Transition*> &Etat::get_vers_Transition() {
	return m_vers_Transition;
}

//les setters
void Etat::setMindexEtat(int MindexEtat) {
	m_indexEtat = MindexEtat;
}
void Etat::setMinitial(bool Minitial) {
	m_initial = Minitial;
}

void Etat::setMacceptant(bool Macceptant) {
	m_acceptant = Macceptant;
}
void Etat::set_vers_transition(std::vector<Transition*> &Mvers_Transition) {
	m_vers_Transition = Mvers_Transition;
}