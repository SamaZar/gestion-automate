#pragma once
#include"Automate.h"
#include"Etat.h"

class Transition{

protected:
	Etat* m_target;
	char m_alphabet;
	//int m_nbTransition;
	

public:
	Transition();
	Transition(char alphabet, Etat* target);
	~Transition();

	//les getters 

	char getMalphabet();
	Etat* getMtarget();

	//les setters

	void setMalphabet(char Malphabet);
	void setMtarget(Etat* Mtarget);
};