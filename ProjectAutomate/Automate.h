#pragma once
#include<iostream>
#include<vector>
#include"Etat.h"
#include"Transition.h"
class Automate {

protected:
	std::string m_alphabet;
	std::vector<Etat*> m_automate_etat;
	std::vector<Transition*> m_automate_transition;

public:
	Automate();//Constructeur
	Automate(std::string File[]);//Cr�ation du fichier
	~Automate();

		//les getters
	const std::string &getMalphabet();
	std::vector<Etat*> getAuto_Etat();
	std::vector<Transition*> getAuto_Transition();
		//les setters

	void setMalphabet(std::string &Malphab);
	void setMauto_Etat(std::vector<Etat*> &Mautmate_etat);
	void setMauto_transition(std::vector<Transition*> &Mautomate_transition);

	//m�thodes
	void initialisation(std::string File[]);
};