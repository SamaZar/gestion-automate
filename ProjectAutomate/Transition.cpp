#include"Transition.h"

Transition::Transition() {

}

Transition::Transition(char alphabet, Etat* target) {
	m_alphabet = alphabet;
	m_target = target;
}

Transition::~Transition() {

}

//les getters 

char Transition::getMalphabet() {
	return m_alphabet;
}
Etat* Transition::getMtarget() {
	return m_target;
}

//les setters

void Transition::setMalphabet(char Malphabet) {
	m_alphabet = Malphabet;
}
void Transition::setMtarget(Etat* Mtarget) {
	m_target = Mtarget;
}